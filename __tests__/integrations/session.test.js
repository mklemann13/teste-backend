const request = require('supertest');

const app = require('../../src/app');

const factory = require('../factoty');

const truncate = require('../utils/truncate');

describe('Authentication', () => {
    beforeEach(async () => {
        await truncate();
    });

    it('should authenticate with valid credentials', async () => {

        const user = await factory.create('User', {
            password: '123123'
        });

        const response = await request(app)
            .post('/sessions')
            .send({
                email: user.email,
                password: '123123'
            });

        expect(response.status).toBe(200);

    });

    it('should not authenticate with invalid credentials', async () => {

        await factory.create('User', {
            password: '32132131'
        });

        const response = await request(app)
            .post('/sessions')
            .send({
                email: "mklemann@live.com",
                password: '1234566'
            });

        expect(response.status).toBe(401);

    });

    it('should be able to access privete routes when authenticated', async () => {
        const user = await factory.create('User', {
            password: '32132131'
        });


        const response = await request(app)
            .get('/movies')
            .set('Authorization', `Bearer ${user.generateToken()}`);

        expect(response.status).toBe(200);
    });

    it('should not be able to access privetes without jwt token', async () => {
        const user = await factory.create('User', {
            password: '32132131'
        });

        const response = await request(app)
            .get('/movies')

        expect(response.status).toBe(401);

    });

});