module.exports = {
    bail: true, 

    clearMocks: true,

    testEnvironment: 'node',

    testMatch: [
        "**/__tests__/**/*.test.js?(x)"
    ],

    collectCoverage: true,

    collectCoverageFrom: ["src/**"],

    coverageDirectory: "__tests__/coverage"
};