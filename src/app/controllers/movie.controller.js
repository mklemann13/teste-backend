const { Movie, User, Vote } = require('../models');
const bcrypt = require('bcryptjs');

class ControllerController {
    async create(req, res) {

        const { name, director, genry, authors } = req.body;
        const { userId } = req;

        const { admin } = await User.findOne({ where: { id: userId, deleted: false } });
        if (!admin) {
            return res.status(403).json({ message: 'Invalid permissions!' });
        };

        if (!name || !director || !genry || !authors) {
            return res.status(500).json({ message: 'Invalid fields, review and try again!' });
        };

        const movieCreated = await Movie.create({ name, director, genry, authors });

        return res.json(movieCreated);
    }

    async list(req, res) {

        const { name, director, genry, authors } = req.query;

        const configFilters = {
            name: name ? name : null,
            director: director ? director : null,
            genry: genry ? genry : null,
            authors: authors ? authors : null
        };

        Object.keys(configFilters).forEach((key) => (configFilters[key] == null) && delete configFilters[key]);

        const movie = await Movie.findAll({ where: configFilters });

        return res.json(movie);
    }

    async details(req, res) {

        const { id } = req.params;

        if (!id) {
            return res.status(500).json({ message: 'Id must be send!' })
        }

        const score = await Vote.findAll({ where: { movie_id: id } });

        const averageScore = score.reduce((acc, current) => acc + current.score, 0)

        const movie = await Movie.findOne({ where: { id } });
        movie['score'] = Number(averageScore)/score.length

        return res.json(movie);
    }

};

module.exports = new ControllerController();