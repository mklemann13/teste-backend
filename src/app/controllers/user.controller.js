const { User } = require('../models');
const bcrypt = require('bcryptjs');

class ControllerController {
    async create(req, res) {

        const { name, email, password, admin } = req.body;

        const user = await User.findOne({ where: { email } });
        if (user) {
            return res.status(400).json({ message: 'User has already been registrered!' })
        }

        if (!name || !email || !password) {
            return res.status(500).json({ message: 'Invalid fields, review and try again!' });
        };

        if (typeof admin !== 'boolean') {
            return res.status(500).json({ message: 'Field admin must boolean type!' })
        };


        const { id, deleted } = await User.create({ name, email, password, admin });

        return res.json({ id, name, email, password, admin, deleted });
    }

    async delete(req, res) {

        const { id } = req.params;

        if (!id) {
            return res.status(400).json({ message: 'Id not provided!' });
        }

        await User.update(
            { deleted: true },
            { where: { id } }
        );

        return res.json({ message: 'success' });
    }

    async update(req, res) {

        const { id } = req.params;
        const { name, email, password, admin } = req.body;

        if (!id) {
            return res.status(400).json({ message: 'Id not provided!' });
        }

        if (password) {
            const password_hash = await bcrypt.hash(password, 8);

            await User.update(
                { name, email, password_hash, admin },
                { where: { id } }
            );

            return res.json({ message: 'success' });
        }

        const user = await User.update(
            { name, email, password_hash, admin },
            { where: { id } }
        );

        return res.json({ message: 'success' });
    }

};

module.exports = new ControllerController();