const { Vote, User } = require('../models');

class ControllerController {
    async create(req, res) {

        const { user_id, movie_id, score } = req.body;
        const { userId } = req;

        console.log({userId});

        if (score < 0 || score > 4) {
            return res.status(500).json({ message: 'The score must be between 0 and 4' })
        };

        if (!user_id || !movie_id) {
            return res.status(500).json({ message: 'The scire must be between 0 and 4' })
        };

        const { admin } = await User.findOne({ where: { id: userId } });
        if (admin) {
            return res.status(403).json({ message: 'Invalid permissions!' });
        };

        const checkHasVoted = await Vote.findOne({ where: { user_id, movie_id } });
        if (checkHasVoted) {
            return res.status(500).json({ message: 'User has already voted for this movie' });
        };

        const createdVote = await Vote.create(req.body);

        return res.json(createdVote);
    }

};

module.exports = new ControllerController();