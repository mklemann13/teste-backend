module.exports = (sequelize, DataTypes) => {
    const Movie = sequelize.define('Movie', {
        name: DataTypes.STRING,
        score: DataTypes.STRING,
        director: DataTypes.STRING,
        genry: DataTypes.STRING,
        authors: DataTypes.STRING
    });

    Movie.associate = (models) => {
        Movie.belongsToMany(models.Vote, {
            through: 'votes',
            foreignKey: 'movie_id'
        });
    };

    return Movie;
};