module.exports = (sequelize, DataTypes) => {
    const Vote = sequelize.define('Vote', {
        score: DataTypes.INTEGER,
        user_id: DataTypes.INTEGER,
        movie_id: DataTypes.INTEGER
    });

    return Vote;
};