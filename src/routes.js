const routes = require('express').Router();

const authMiddleware = require('./middlewares/auth');

const sessionController = require('../src/app/controllers/session.controller');
const userController = require('../src/app/controllers/user.controller');
const movieController = require('./app/controllers/movie.controller');
const voteController = require('../src/app/controllers/votes.controller');

routes.post('/sessions', (req, res) => sessionController.store(req, res));

routes.use(authMiddleware);

routes.post('/users', (req, res) => userController.create(req, res));
routes.delete('/users/:id', (req, res) => userController.delete(req, res));
routes.put('/users/:id', (req, res) => userController.update(req, res));

routes.post('/movies', (req, res) => movieController.create(req, res));
routes.get('/movies', (req, res) => movieController.list(req, res));
routes.get('/movies/:id', (req, res) => movieController.details(req, res));

routes.post('/votes', (req, res) => voteController.create(req, res));

module.exports = routes;